#include "Tile.h"
#include "ofMain.h"


Tile::Tile(int id, int height, int width, ofTexture* image)
{
    this->id = id;
    this->image = image;
    this->height = height;
    this->width = width;
    this->image = image;
    this->selected = false;
}


Tile::~Tile()
{
}

int Tile::getX() {
    return x;
}

int Tile::getY() {
    return y;
}

void Tile::setX(int x) {
    this->x = x;
}

void Tile::setY(int y) {
    this->y = y;
}

void Tile::setIsoX(int x) {
    this->isoX = x;
}

void Tile::setIsoY(int y) {
    this->isoY = y;
}

int Tile::getWidth() {
    return this->width;
}

int Tile::getHeight() {
    return this->height;
}

bool Tile::getSelected() {
    return this->selected;
}

void Tile::setSelected(bool selected) {
    this->selected = selected;
}

void Tile::setImage(ofTexture* image) {
    this->image = image;
}

void Tile::draw() {    
    
    // Different tiles may have a different image height
    int imageHeight;

    switch (id) {
    case 0:
        imageHeight = 65;
        break;
    case 1:
        imageHeight = 66;
        break;
    case 2:
        imageHeight = 64;
        break;
    case 3:
        imageHeight = 66;
        break;
    default:
        imageHeight = 65;
    }

    image->setAnchorPoint(width, 0);
    image->draw(isoX, isoY, 2 * width, 65);

  //  ofSetColor(ofColor::white);
  //  ofDrawRectangle(isoX, isoY, 3, 3);   
}