#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){

    ofSetBackgroundColor(ofColor::black);

    // Load TEXTURES
    string file_grass = "grass.png";
    string file_dirt = "dirt.png";
    string file_water = "water.png";
    string file_dirt_selected = "dirt_selected.png";
    ofLoadImage(grass, file_grass);
    ofLoadImage(dirt, file_dirt);
    ofLoadImage(water, file_water);
    ofLoadImage(dirt_selected, file_dirt_selected);

    // Offset for the isometric coordinates (grid should be centered on the screen)
    gridOffsetX = 0.5*ofGetWindowWidth();
    gridOffsetY = 0.25*ofGetWindowHeight();

    // Create a tile object for every entry in the map
    for (int x = 0; x < numTilesX; x++) {
        for (int y = 0; y < numTilesY; y++) {
            
            // Get tile id and use appropriate texture
            int id = map[x][y];
            ofTexture* texture;
            switch (id) {
            case 0:
                texture = &grass;
                break;
            case 1:
                texture = &dirt;
                break;
            case 2:
                texture = &water;
                break;
            case 3:
                texture = &dirt;
                break;
            default:
                texture = &grass;
                break;
            }

            // CREATE TILE
            Tile* tile = new Tile(id, tileHeight, tileWidth, texture);
            
            
            // SET CARTESIAN COORDINATES   
            int cartesianX = x*tileWidth;
            int cartesianY = y*tileHeight;
            tile->setX(cartesianX);
            tile->setY(cartesianY);


            // SET ISOMETRIC COORDINATES (+ OFFSET)
            int isoX = toIsoX(cartesianX, cartesianY) + gridOffsetX;
            int isoY = toIsoY(cartesianX, cartesianY) + gridOffsetY;
            tile->setIsoX(isoX);
            tile->setIsoY(isoY);

            tiles.push_back(tile);
        }
    }
}

//--------------------------------------------------------------
void ofApp::update(){


}

int ofApp::toIsoX(int x, int y) {
    return x - y;
}

int ofApp::toIsoY(int x, int y) {
    return (x + y) / 2;
}

int ofApp::toCartesianX(int x, int y) {
    return ((2 * y + x) / 2);
}

int ofApp::toCartesianY(int x, int y) {
    return ((2 * y - x) / 2);
}


void ofApp::selectTile(Tile* tile) {
    // First, unselect all tiles and then select the new one
    for (int i = 0; i < tiles.size(); i++) {
        tiles.at(i)->setSelected(false);  
        tiles.at(i)->setImage(&dirt);
    }

    tile->setImage(&dirt_selected);
    tile->setSelected(true);
}

Tile* ofApp::getTileByCoords(int x, int y, int tileHeight) {

    Tile* foundTile = nullptr;

    // Find tile corresponding to given coordinates
    for (int i = 0; i < tiles.size(); i++) {

        Tile* curTile = tiles.at(i);
        if ((x < curTile->getX() + curTile->getWidth() && x > curTile->getX())
            && (y < curTile->getY() + curTile->getHeight() && y > curTile->getY())) {
            foundTile = curTile;
        }
    }
    return foundTile;
}

//--------------------------------------------------------------
void ofApp::draw(){
    for (int i = 0; i < tiles.size(); i++) {
        tiles.at(i)->draw();
    }

    ostringstream sstream;
    ostringstream sstream2;

    sstream << "Screen: " << ofGetMouseX() << " " << ofGetMouseY();
    sstream2 << "Iso grid coords: " << toCartesianX(ofGetMouseX()-gridOffsetX, ofGetMouseY()-gridOffsetY) << " " << toCartesianY(ofGetMouseX()-gridOffsetX, ofGetMouseY()-gridOffsetY);
    ofDrawBitmapString(sstream.str(), 600, 100);
    ofDrawBitmapString(sstream2.str(), 600, 200);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

    // Check mouse coordinates and highlight the tile under the cursor
    Tile* tile = getTileByCoords(toCartesianX(x-gridOffsetX, y-gridOffsetY), toCartesianY(x-gridOffsetX, y-gridOffsetY), tileHeight);
    if (tile != nullptr)
        selectTile(tile);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
