#pragma once

#include "ofMain.h"
#include "..\Tile.h"
class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    void selectTile(Tile* tile);
    Tile* getTileByCoords(int x, int y, int tileHeight);

    ofTexture grass;
    ofTexture dirt;
    ofTexture water;
    ofTexture dirt_selected;

    int toIsoX(int x, int y);
    int toIsoY(int x, int y);
    int toCartesianX(int x, int y);
    int toCartesianY(int x, int y);

    int gridOffsetX;
    int gridOffsetY;

    static const int numTilesX = 6;
    static const int numTilesY = 6;

    const int map[numTilesX][numTilesY] = {
        {1,1,1,1,1,1},
        {1,1,1,1,1,1},
        {1,1,1,1,1,1},
        {1,1,1,1,1,1},
        {1,1,1,1,1,1},
        {1,1,1,1,1,1}
    };

    vector<Tile*> tiles;

    const int tileWidth = 50;
    const int tileHeight = 50;
};
