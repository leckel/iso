#pragma once
#include "ofTexture.h"
class Tile
{
public:
    Tile(int id, int height, int width, ofTexture* image);
    ~Tile();
    int getX();
    int getY();
    int getWidth();
    int getHeight();
    bool getSelected();


    void setSelected(bool selected);
    void setX(int x);
    void setY(int y);
    void setIsoX(int x);
    void setIsoY(int y);
    void setImage(ofTexture* image);

    void draw();

private:
    int id;
    ofTexture* image;
    int x;
    int y;
    int isoX;
    int isoY;
    int height;
    int width;
    bool selected;
};

